using Microsoft.EntityFrameworkCore;
using System.Net.Http.Json;
using System.Security.Permissions;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        MakeDB Database;
        List<Item> Items;
        ListBox listaWyboruItemow;
        int KtoraStr = 1;
        int ElemNaStr = 30;
        public Form1()
        {
            InitializeComponent();
            Database = new MakeDB();
            Items = new List<Item>();
            listaWyboruItemow = listBox1;
            label1.Text = "Podaj ID";
            textBox1.Enabled = false;
            button1.Enabled = false;
            textBox2.Enabled = false;
            textBoxRar.Enabled = false;
            textBoxType.Enabled = false;
            InitItems();
            UstawJakaStr();
        }

        async void UstawJakaStr()
        {
            int iloscStron = Database.Itemy.Count() / ElemNaStr + 1;
            textBoxPage.Text = KtoraStr + 1 + "/" + iloscStron;
        }
        private void UstawItemyWLiscie(int ile, int od = 0)
        {
            if (Database.Itemy.Count() < od + ile)
            {
                ile = Database.Itemy.Count() - od;
            }
            string toAppend = "";
            var itemsToSet = Database.Itemy.ToList();
            listaWyboruItemow.Items.Clear();
            for (int i = od; i < od + ile; i++)
            {
                toAppend = itemsToSet[i].ToString();
                listaWyboruItemow.Items.Add(toAppend);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBoxIleWysw.Text != "")
            {
                ElemNaStr = int.Parse(textBoxIleWysw.Text.ToString());
                if(ElemNaStr <= 0)
                {
                    ElemNaStr = 100;
                }
            }
            else
            {
                ElemNaStr = 100;
            }
            UstawItemyWLiscie(ElemNaStr, KtoraStr * ElemNaStr);
            UstawJakaStr();
        }
        private async void InitItems()
        {
            int pom = 0;
            List<int> ids = new List<int>();
            if (Database.Itemy.Count() > 0)
            {
                foreach (Item elem in Database.Itemy.ToList())
                {
                    if (elem.id > pom)
                    {
                        pom = elem.id;
                    }

                }
            }
            string ItemJson = await GetAllItemsID();

            string[] numbers = Regex.Split(ItemJson, @"\D+");
            foreach (string value in numbers)
            {
                if ((!string.IsNullOrEmpty(value)))
                {
                    int nid = int.Parse(value);
                    if (pom < nid)
                        ids.Add(nid);
                    textBox2.Text = nid.ToString();
                }
            }
            //textBox2.Text = ids.ToString();
            textBox1.Enabled = true;
            button1.Enabled = true;
            SetUpItemsByID(ids);
        }
        private async void button1_Click(object sender, EventArgs e)
        {
            int szukane_id;
            try
            {
                szukane_id = int.Parse(textBox1.Text);
            }catch(Exception)
            {
                szukane_id = 0;
            }
            foreach (Item item in Database.Itemy.ToList())
            {
                if (item.id == szukane_id)
                {
                    textBox2.Text = item.name;
                    pictureBox1.LoadAsync(item.icon);
                    textBoxRar.Text = item.rarity;
                    textBoxType.Text = item.type;
                    //Uri img = new Uri(item.icon);

                }
            }


        }

        private async void SetUpItemsByID(List<int> ids)
        {
            foreach (int ID in ids)
            {
                string call = "https://api.guildwars2.com/v2/items/" + ID.ToString();
                HttpClient client = new HttpClient();
                string response = await client.GetStringAsync(call);
                Item item_data = JsonSerializer.Deserialize<Item>(response);
                Database.Itemy.Add(item_data);
                Database.SaveChanges();
            }
        }
        private async Task<string> GetAllItemsID()
        {
            HttpClient client = new HttpClient();
            try
            {
                string call = "https://api.guildwars2.com/v2/items";
                HttpResponseMessage resp = await client.GetAsync(call);
                string json = await client.GetStringAsync(call);
                return json;
            }
            catch (HttpRequestException ex)
            {
                return "";
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (KtoraStr > 0)
            {
                KtoraStr--;
            }
            UstawJakaStr();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void buttonPlusPage_Click(object sender, EventArgs e)
        {
            int MaxStr = Database.Itemy.Count() / ElemNaStr;
            if (KtoraStr < MaxStr)
            {
                KtoraStr++;
            }
            UstawJakaStr();
        }
    }
}