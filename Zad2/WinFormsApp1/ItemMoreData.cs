﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    internal class ItemMoreData : Item
    {
        public string type { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
}
