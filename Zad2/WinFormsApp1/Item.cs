﻿using System;

public class Item
{
	public int id { get; set; }
	public string name { get; set; }
	public string type { get; set; }
	public string icon { get; set; }

	public string rarity { get; set; }


    public Item()
	{
	}

    public override string ToString()
    {
		return "" + id + ": " + name;
    }
}
