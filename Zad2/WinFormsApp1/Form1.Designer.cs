﻿namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            textBox1 = new TextBox();
            textBox2 = new TextBox();
            listBox1 = new ListBox();
            label1 = new Label();
            Label2 = new Label();
            pictureBox1 = new PictureBox();
            textBoxRar = new TextBox();
            label3 = new Label();
            textBoxType = new TextBox();
            label4 = new Label();
            buttonShowItems = new Button();
            textBoxIleWysw = new TextBox();
            label5 = new Label();
            textBoxPage = new TextBox();
            buttonMinusPage = new Button();
            buttonPlusPage = new Button();
            label6 = new Label();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(632, 22);
            button1.Name = "button1";
            button1.Size = new Size(156, 78);
            button1.TabIndex = 0;
            button1.Text = "Search";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(417, 50);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(141, 31);
            textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(417, 255);
            textBox2.Multiline = true;
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(371, 39);
            textBox2.TabIndex = 2;
            // 
            // listBox1
            // 
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 25;
            listBox1.Location = new Point(12, 9);
            listBox1.Name = "listBox1";
            listBox1.Size = new Size(399, 379);
            listBox1.TabIndex = 3;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(417, 22);
            label1.Name = "label1";
            label1.Size = new Size(59, 25);
            label1.TabIndex = 4;
            label1.Text = "label1";
            label1.Click += label1_Click;
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new Point(417, 224);
            Label2.Name = "Label2";
            Label2.Size = new Size(59, 25);
            Label2.TabIndex = 5;
            Label2.Text = "Name";
            // 
            // pictureBox1
            // 
            pictureBox1.Location = new Point(632, 116);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(156, 133);
            pictureBox1.TabIndex = 6;
            pictureBox1.TabStop = false;
            pictureBox1.Click += pictureBox1_Click_1;
            // 
            // textBoxRar
            // 
            textBoxRar.Location = new Point(417, 325);
            textBoxRar.Multiline = true;
            textBoxRar.Name = "textBoxRar";
            textBoxRar.Size = new Size(184, 39);
            textBoxRar.TabIndex = 7;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(417, 297);
            label3.Name = "label3";
            label3.Size = new Size(52, 25);
            label3.TabIndex = 8;
            label3.Text = "rarity";
            label3.Click += label3_Click;
            // 
            // textBoxType
            // 
            textBoxType.Location = new Point(607, 325);
            textBoxType.Multiline = true;
            textBoxType.Name = "textBoxType";
            textBoxType.Size = new Size(181, 39);
            textBoxType.TabIndex = 9;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(608, 298);
            label4.Name = "label4";
            label4.Size = new Size(47, 25);
            label4.TabIndex = 10;
            label4.Text = "type";
            // 
            // buttonShowItems
            // 
            buttonShowItems.Location = new Point(276, 394);
            buttonShowItems.Name = "buttonShowItems";
            buttonShowItems.Size = new Size(135, 44);
            buttonShowItems.TabIndex = 11;
            buttonShowItems.Text = "ShowItems";
            buttonShowItems.UseVisualStyleBackColor = true;
            buttonShowItems.Click += button2_Click;
            // 
            // textBoxIleWysw
            // 
            textBoxIleWysw.Location = new Point(129, 401);
            textBoxIleWysw.Name = "textBoxIleWysw";
            textBoxIleWysw.Size = new Size(141, 31);
            textBoxIleWysw.TabIndex = 12;
            textBoxIleWysw.TextChanged += textBox3_TextChanged;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(23, 404);
            label5.Name = "label5";
            label5.Size = new Size(100, 25);
            label5.TabIndex = 13;
            label5.Text = "Ile pokazac";
            label5.Click += label5_Click;
            // 
            // textBoxPage
            // 
            textBoxPage.Enabled = false;
            textBoxPage.Location = new Point(429, 401);
            textBoxPage.Name = "textBoxPage";
            textBoxPage.Size = new Size(47, 31);
            textBoxPage.TabIndex = 14;
            textBoxPage.TextChanged += textBox3_TextChanged_1;
            // 
            // buttonMinusPage
            // 
            buttonMinusPage.Location = new Point(530, 398);
            buttonMinusPage.Name = "buttonMinusPage";
            buttonMinusPage.Size = new Size(39, 37);
            buttonMinusPage.TabIndex = 17;
            buttonMinusPage.Text = "-";
            buttonMinusPage.UseVisualStyleBackColor = true;
            buttonMinusPage.Click += button3_Click;
            // 
            // buttonPlusPage
            // 
            buttonPlusPage.Location = new Point(485, 398);
            buttonPlusPage.Name = "buttonPlusPage";
            buttonPlusPage.Size = new Size(39, 37);
            buttonPlusPage.TabIndex = 18;
            buttonPlusPage.Text = "+";
            buttonPlusPage.UseVisualStyleBackColor = true;
            buttonPlusPage.Click += buttonPlusPage_Click;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(417, 373);
            label6.Name = "label6";
            label6.Size = new Size(50, 25);
            label6.TabIndex = 19;
            label6.Text = "Page";
            label6.Click += label6_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(label6);
            Controls.Add(buttonPlusPage);
            Controls.Add(buttonMinusPage);
            Controls.Add(textBoxPage);
            Controls.Add(label5);
            Controls.Add(textBoxIleWysw);
            Controls.Add(buttonShowItems);
            Controls.Add(label4);
            Controls.Add(textBoxType);
            Controls.Add(label3);
            Controls.Add(textBoxRar);
            Controls.Add(pictureBox1);
            Controls.Add(Label2);
            Controls.Add(label1);
            Controls.Add(listBox1);
            Controls.Add(textBox2);
            Controls.Add(textBox1);
            Controls.Add(button1);
            Name = "Form1";
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private TextBox textBox1;
        private TextBox textBox2;
        private ListBox listBox1;
        private Label label1;
        private Label Label2;
        private PictureBox pictureBox1;
        private TextBox textBoxRar;
        private Label label3;
        private TextBox textBoxType;
        private Label label4;
        private Button buttonShowItems;
        private TextBox textBoxIleWysw;
        private Label label5;
        private TextBox textBoxPage;
        private Button buttonMinusPage;
        private Button buttonPlusPage;
        private Label label6;
    }
}