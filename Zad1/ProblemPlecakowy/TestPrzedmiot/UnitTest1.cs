﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ProblemPlecakowy;
using System.Collections.Generic;

namespace TestPrzedmiot
{
    [TestClass]
    public class UnitTest1
    {
        ProblemPlecakowy.ProblemPlecakowy problem;
        ProblemPlecakowy.ProblemPlecakowy problem2;

        [TestMethod]
        public void TestBrakuRozwPrzedRozwProblemu()
        {
            problem = new ProblemPlecakowy.ProblemPlecakowy(123, 1000);

            Assert.AreNotEqual(problem.Przedmioty.Count, 0);

            Assert.IsNull(problem.RozwionzanieProblemu());

            Assert.IsNull(problem.KtoreIdWybrano());
        }

        [TestMethod]
        public void TestMniejItemowNizPojemnosci()
        {
            int ilosc_itemow = 10;
            double rozmiar_plecaka = (double)ilosc_itemow * 20.0;

            problem = new ProblemPlecakowy.ProblemPlecakowy(18492, 10);
            problem.UstawWarunkiRozw(1000);
            problem.RozwionzanieProblemu();

            Assert.AreEqual(problem.WybraneItemy.Count, 10);
            for (int id = 0; id < ilosc_itemow; id++)
            {
                Assert.IsTrue(problem.WybraneItemy.Contains(id));
            }
        }

        void SwapItems(int id1, int id2, ProblemPlecakowy.ProblemPlecakowy problem)
        {
            double pom_wart, pom_waga, pom_wart_wag;
            pom_waga = problem.Przedmioty[id1].waga;
            pom_wart = problem.Przedmioty[id1].wartosc;
            pom_wart_wag = problem.Przedmioty[id1].wart_wag;
            problem.Przedmioty[id1].waga = problem.Przedmioty[id2].waga;
            problem.Przedmioty[id1].wartosc = problem.Przedmioty[id2].wartosc;
            problem.Przedmioty[id1].wart_wag = problem.Przedmioty[id2].wart_wag;
            problem.Przedmioty[id2].waga = pom_waga;
            problem.Przedmioty[id2].wartosc = pom_wart;
            problem.Przedmioty[id2].wart_wag = pom_wart_wag;
            problem.Przedmioty[id2].id = id1;
            problem.Przedmioty[id1].id = id2;


        }

        [TestMethod]
        public void TestRozwianzanie_aInaczejUlorzoneDane()
        {
            int ilosc_itemow = 10;
            problem = new ProblemPlecakowy.ProblemPlecakowy(0, ilosc_itemow);
            problem2 = new ProblemPlecakowy.ProblemPlecakowy(0, ilosc_itemow);
            for (int i = 0; i < (ilosc_itemow / 2); i++)
            {
                SwapItems(i, ilosc_itemow - i - 1, problem2);
            }

        }

    }
}