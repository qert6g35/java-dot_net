﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("TestPrzedmiot")]
[assembly: InternalsVisibleTo("WindowsFormsProblemPlecakowy")]

namespace ProblemPlecakowy
{
    internal class ProblemPlecakowy
    {
        public List<Przedmiot> Przedmioty { get;}
        //public List<int> CzyItemWPlecaku { get; set; }
        public List<int> WybraneItemy { get; set; }
        double MaxDostWaga;
        double MaxWaga;
        bool CzyRozwProblem = false;

        public ProblemPlecakowy(int seed,int ilosc_item){
            Przedmioty = new List<Przedmiot>();
            for(int i = 0; i < ilosc_item; i++)
            {
                Przedmioty.Add(new Przedmiot(seed, i));
                //CzyItemWPlecaku.Add(0);
                seed += (i + 1);
            }

        }

        public string PokazProblem()
        {
            string pom = "";
            foreach (var item in Przedmioty)
            {
              pom = pom + item.ToString();
                pom = pom + "\n";
            }
            return pom;
        }
        public void UstawWarunkiRozw(double _MaxWaga)
        {
            MaxDostWaga = _MaxWaga;
            MaxWaga = _MaxWaga;
        }

        public List<int> KtoreIdWybrano()
        {
            if (CzyRozwProblem)
            {
                return WybraneItemy;
            }
            else
            {
                return null;
            }
        }
        public string RozwionzanieProblemu()
        {
            if (MaxDostWaga <= 0)
            {
                return null;
            }
            string Rozw = "";
            WybraneItemy = new List<int>();
            double pom_najw_wartosc = 0;
            double pom_ostatnia_najw_w = 1000;
            int pomid;
            bool end = false;
            while (!end)
            {
                pom_najw_wartosc = 0;
                pomid = -1;
                foreach (var item in Przedmioty)
                {
                    if ((item.wart_wag > pom_najw_wartosc) && !(WybraneItemy.Contains(item.id)))
                    {
                        pom_najw_wartosc = item.wart_wag;
                        pomid = item.id;
                    }
                }
                if (pomid == -1)
                {
                    end = true;
                }
                else
                {

                    MaxDostWaga -= Przedmioty[pomid].waga;
                    if (MaxDostWaga >= 0)
                    {
                        pom_ostatnia_najw_w = Przedmioty[pomid].wart_wag;
                        WybraneItemy.Add(pomid);
                    }
                    else
                    {
                        MaxDostWaga += Przedmioty[pomid].waga;
                        end = true;
                    }
                }
            }
            foreach (var item in WybraneItemy)
            {
                Rozw = Rozw + Przedmioty[item].ToString();
                Rozw += "\n";
                    }
            double pom_waga_item = MaxWaga - MaxDostWaga;
            CzyRozwProblem = true;
            Rozw += "Waga przedmiotów i pojemność plecaka:" + pom_waga_item.ToString() + "/" + MaxWaga.ToString();
            return Rozw;
        } 

    }
}