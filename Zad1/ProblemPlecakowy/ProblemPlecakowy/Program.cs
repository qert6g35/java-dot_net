﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemPlecakowy
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Podaj jaka ma być ilosc itemów:");
            int ilosc_item = Convert.ToInt32(Console.ReadLine());
            Console.Write("Podaj jakiś seed:");
            int seed = Convert.ToInt32(Console.ReadLine());

            ProblemPlecakowy Problem = new ProblemPlecakowy(seed,ilosc_item);

            Console.WriteLine( Problem.PokazProblem());
            Console.WriteLine("Podaj jaka ma być pojremność plecaka:");
            double wybrana_waga = Convert.ToDouble(Console.ReadLine());
            Problem.UstawWarunkiRozw(wybrana_waga);
            Console.WriteLine(Problem.RozwionzanieProblemu());
            Console.ReadKey();
        }
    }
}
