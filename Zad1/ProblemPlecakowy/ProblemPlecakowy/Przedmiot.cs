﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemPlecakowy
{
    internal class Przedmiot
    {
        generator randomizer;
        public int id { set; get; }
        public double waga { get; set; }
        public double wartosc { get; set; }
        public double wart_wag { get; set; }


        public Przedmiot(int seed, int newid){
            randomizer = new generator(seed);
            waga = ((double)randomizer.rand(10 , 100) / 10.0);
            wartosc = ((double)randomizer.rand(100, 1000) / 10.0);
            wart_wag = wartosc / waga;
            id = newid;
        }

        public override string ToString()
        {
            string str ="id:"+ id.ToString() +" waga:" + waga.ToString()+ " wart:" + wartosc.ToString() + " wart/wag: "+ wart_wag.ToString();
            return str;
        }
    }
}
