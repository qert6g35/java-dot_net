﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProblemPlecakowy;

namespace WindowsFormsProblemPlecakowy
{
    public partial class Form1 : Form
    {
        ProblemPlecakowy.ProblemPlecakowy ProblemPlec;
        bool clickButton2;
        public Form1()
        {
            InitializeComponent();
            clickButton2 = true;
            textBox3.Enabled = false;
            button2.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if ((textBox2.Text != "") && (textBox1.Text != ""))
            {
                int Ilosc_item = int.Parse(textBox1.Text);
                int Seed = int.Parse(textBox2.Text);
                ProblemPlec = new ProblemPlecakowy.ProblemPlecakowy(Seed,Ilosc_item);
                foreach (var item in ProblemPlec.Przedmioty)
                    listBox1.Items.Add(item);
                button2.Enabled = true;
                textBox3.Enabled=true;
            }
        }// generate

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (clickButton2)
            {
                listBox1.Items.Clear();
                button2.Text = "RESET";
                button1.Enabled = false;
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                clickButton2 = false;
                int poj = int.Parse(textBox3.Text);
                ProblemPlec.UstawWarunkiRozw(poj);
                ProblemPlec.RozwionzanieProblemu();
                foreach(int id in ProblemPlec.WybraneItemy)
                {
                    listBox1.Items.Add(ProblemPlec.Przedmioty[id]);
                }
            }
            else
            {
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                button2.Text = "Solve !";
                listBox1.Items.Clear();
                button1.Enabled = true;
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                clickButton2 = true;
                textBox3.Enabled = false;
                button2.Enabled = false;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }// ilosc itemow

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
