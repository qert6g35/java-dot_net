﻿using System;
using System.Threading;
using System.Diagnostics;

class Siema
{
    static void Main()
    {
        int watki = 4;
        long iteracje = 1000000000; //10 miliardow ok. 2min15sek, miliard 1 watek 30 sek, 4 watki 13 sek
        long iter_na_watek = iteracje / watki;
        double suma = 0, liczba_pi = 0;
        double real_pi = 3.141592653589793238;
        Console.WriteLine("Prawdziwa liczba Pi wynosi: {0}", real_pi);

        Console.WriteLine("Obliczanie bez wątków: ");
        Stopwatch stopwatch = Stopwatch.StartNew();
        Pi_Monte_Carlo PI = new Pi_Monte_Carlo(iteracje);
        PI.Oblicz();

        liczba_pi = 4 * PI.licznik / iteracje;
        stopwatch.Stop();
        Console.WriteLine("Liczba Pi wynosi w przybliżeniu: {0}", liczba_pi);
        Console.WriteLine("Czas wykonania bez wątków: {0} ms", stopwatch.ElapsedMilliseconds);
        //Console.ReadLine();


        Console.WriteLine("Obliczanie z wątkami: ");
        stopwatch = Stopwatch.StartNew();
        liczba_pi = 0;
        Pi_Monte_Carlo[] pi = new Pi_Monte_Carlo[watki];
        Thread[] threads = new Thread[watki];
        //Console.WriteLine("test");
        for (int i = 0; i < watki; i++)
        {
            pi[i] = new Pi_Monte_Carlo(iter_na_watek);
            threads[i] = new Thread(pi[i].Oblicz);
            threads[i].Start();
        }
        //Console.WriteLine("test");
        for (int i = 0; i < watki; i++)
        {
            threads[i].Join();
            suma += pi[i].licznik;
        }

        liczba_pi = 4 * suma / iteracje;
        stopwatch.Stop();
        Console.WriteLine("Liczba Pi wynosi w przybliżeniu: {0}", liczba_pi);
        Console.WriteLine("Czas wykonania z wątkami: {0} ms", stopwatch.ElapsedMilliseconds);
        Console.ReadLine();
    }
}
public class Pi_Monte_Carlo
{
    private long iteracje;
    public double licznik;
    private Random rand;

    public Pi_Monte_Carlo(long iteracje)
    {
        this.iteracje = iteracje;
        this.licznik = 0;
        this.rand = new Random();
    }

    public void Oblicz()
    {
        for (long i = 0; i < iteracje; i++)
        {
            double x = rand.NextDouble();
            double y = rand.NextDouble();

            if (x * x + y * y <= 1)
            {
                licznik++;
            }
        }
    }
}